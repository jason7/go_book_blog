package global

import (
	"gitee.com/jason7/go_book_blog/pkg/logger"
	"gitee.com/jason7/go_book_blog/pkg/setting"
)

var (
	ServerSetting    *setting.ServerSettingS
	AppSetting       *setting.AppSettingS
	DatabasesSetting *setting.DatabaseSettingS
	Logger           *logger.Logger
	JWTSetting       *setting.JWTSettingS
	EmailSetting     *setting.EmailSettingS
)
