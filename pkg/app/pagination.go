package app

import (
	"gitee.com/jason7/go_book_blog/global"
	"gitee.com/jason7/go_book_blog/pkg/convert"
	"github.com/gin-gonic/gin"
)

//GetPage 获取页码
func GetPage(c *gin.Context) int {
	page := convert.StrTo(c.Query("page")).MustInt()
	if page <= 0 {
		return 1
	}
	return page
}

//GetPageSize 获取每页数据条数
func GetPageSize(c *gin.Context) int {
	pageSize := convert.StrTo(c.Query("page_size")).MustInt()
	if pageSize <= 0 {
		return global.AppSetting.DefaultPageSize
	}
	if pageSize > global.AppSetting.MaxPageSize {
		return global.AppSetting.MaxPageSize
	}
	return pageSize
}

//GetPageOffset 获取位移参数
func GetPageOffset(page, pageSize int) int {
	result := 0
	if page > 0 {
		result = (page - 1) * pageSize
	}
	return result
}
