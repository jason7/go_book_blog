package service

import (
	"gitee.com/jason7/go_book_blog/internal/model"
	"gitee.com/jason7/go_book_blog/pkg/app"
)

type CountTagRequest struct {
	Name  string `form:"name" binding:"max=100"`
	State uint8  `form:"state,default=1" binding:"oneof=0 1"`
}

type TagListRequest struct {
	Name  string `form:"name" validate:"max=100"`
	State uint8  `form:"state,default=1" validate:"oneof=0 1"`
}

type CreateTagRequest struct {
	Name      string `form:"name" validate:"required,min=3,max=100"`
	CreatedBy string `form:"created_by" validate:"required,min=3,max=100"`
	State     uint8  `form:"state,default=1" validate:"oneof=0 1"`
}

type UpdateTagRequest struct {
	Id         uint32 `form:"id" validate:"required,get=1"`
	Name       string `form:"name" validate:"min=3,max=100"`
	State      uint8  `form:"state" validate:"required,oneof=0 1"`
	ModifiedBy string `form:"modified_by" validate:"required,min=3,max=100"`
}

type DeleteTagRequest struct {
	Id uint32 `form:"id" json:"id" validate:"required,get=1"`
}

func (svc *Service) CountTag(param *CountTagRequest) (int, error) {
	return svc.dao.CountTag(param.Name, param.State)
}

func (svc *Service) GetTagList(param *TagListRequest, pager *app.Pager) ([]*model.Tag, error) {
	return svc.dao.GetTagList(param.Name, param.State, pager.Page, pager.PageSize)
}

func (svc *Service) CreateTag(param *CreateTagRequest) error {
	return svc.dao.CreateTag(param.Name, param.State, param.CreatedBy)
}

func (svc *Service) UpdateTag(param *UpdateTagRequest) error {
	return svc.dao.UpdateTag(param.Id, param.Name, param.State, param.ModifiedBy)
}

func (svc *Service) DeleteTag(param *DeleteTagRequest) error {
	return svc.dao.DeleteTag(param.Id)
}
