package service

import (
	"context"

	"gitee.com/jason7/go_book_blog/global"

	"gitee.com/jason7/go_book_blog/internal/dao"
)

type Service struct {
	ctx context.Context
	dao *dao.Dao
}

func New(ctx context.Context) Service {
	svc := Service{ctx: ctx}
	svc.dao = dao.New(global.DBEngine)
	return svc
}
